<?php

namespace tbradbury\DynamicsNav\tests;


use tbradbury\DynamicsNav\DynamicsNavClient;
use tbradbury\DynamicsNav\DynamicsNavClientInterface;

/**
 * Class MockDynamicsNavClient.
 */
class MockDynamicsNavClient extends DynamicsNavClient implements DynamicsNavClientInterface
{

    /**
     * Read file contents instead of sending a request to the API.
     *
     * Writes to the file if $data is given.
     *
     * @param string $path
     *   The path to the file to read.
     * @param null $data
     *   Data to write to a file.
     *
     * @return string
     *   The contents of the file.
     */
    public function request($path, $data = null)
    {
        $path = '/objects/SalesOrder.json';
        return file_get_contents(__DIR__ . $path);
    }
}
