<?php

namespace tbradbury\DynamicsNav\Entity;

use tbradbury\DynamicsNav\DynamicsNavClient;
use tbradbury\DynamicsNav\Query\ItemQuery;

/**
 * Class Item.
 */
class Item extends Entity
{

    /**
     * Create an item by querying it from an API.
     *
     * @param \tbradbury\DynamicsNav\DynamicsNavClient $client
     *   The client to connect with.
     * @param string $number
     *   The item's number.
     *
     * @return static
     */
    public static function fromQuery(DynamicsNavClient $client, $number)
    {
        return (new ItemQuery($client, $number))->fetch();
    }
}
