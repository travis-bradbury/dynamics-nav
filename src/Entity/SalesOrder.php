<?php

namespace tbradbury\DynamicsNav\Entity;

use tbradbury\DynamicsNav\DynamicsNavClient;
use tbradbury\DynamicsNav\Query\SalesOrderQuery;

/**
 * Class SalesOrder.
 */
class SalesOrder extends Entity
{

    /**
     * The client to use to communicate with Dynamics Nav.
     *
     * @var \tbradbury\DynamicsNav\DynamicsNavClient
     */
    protected $client;

    /**
     * SalesOrder constructor.
     *
     * @param \tbradbury\DynamicsNav\DynamicsNavClient $client
     *   A client to connect to Dynamcis Nav with.
     */
    public function __construct($data, $client)
    {
      parent::__construct($data);
      $this->client = $client;
    }

    /**
     * Save the Sales Order back to Dynamics Nav.
     *
     * TODO We should get a Sales Order with its properties (like No.) filled out
     * in the response and be able to return a new static here.
     *
     * @param \tbradbury\DynamicsNav\DynamicsNavClient $client
     *   The client to connect to the API with.
     *
     * @return mixed
     *   The response from the API.
     */
    public function save()
    {
        return $this->client->request($this->client->url('SalesOrder'), $this->asJson());
    }

    /**
     * Create an item by querying it from an API.
     *
     * @param \tbradbury\DynamicsNav\DynamicsNavClient $client
     *   The client to connect with.
     * @param string $number
     *   The item's number.
     *
     * @return SalesOrder
     */
    public static function fromQuery(DynamicsNavClient $client, $number)
    {
        return (new SalesOrderQuery($client, $number))->fetch();
    }
}
