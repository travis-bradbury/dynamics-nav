<?php

namespace tbradbury\DynamicsNav\Entity;

use tbradbury\DynamicsNav\DynamicsNavClient;
use tbradbury\DynamicsNav\Query\CustomerQuery;

/**
 * Class Customer.
 */
class Customer extends Entity
{

  /**
   * The client to use to communicate with Dynamics Nav.
   *
   * @var \tbradbury\DynamicsNav\DynamicsNavClient
   */
  protected $client;

  /**
   * Customer constructor.
   *
   * @param \tbradbury\DynamicsNav\DynamicsNavClient $client
   *   A client to connect to Dynamcis Nav with.
   */
  public function __construct($data, $client)
  {
    parent::__construct($data);
    $this->client = $client;
  }

  /**
   * Save the Customer back to Dynamics Nav.
   *
   * @param \tbradbury\DynamicsNav\DynamicsNavClient $client
   *   The client to connect to the API with.
   *
   * @return mixed
   *   The response from the API.
   */
  public function save()
  {
    return $this->client->request($this->client->url('Customer'), $this->asJson());
  }

  /**
   * Create an item by querying it from an API.
   *
   * @param \tbradbury\DynamicsNav\DynamicsNavClient $client
   *   The client to connect with.
   * @param string $number
   *   The item's number.
   * @param string $filterBy
   *   Filter by identifier.
   *
   * @return Customer
   */
  public static function fromQuery(DynamicsNavClient $client, $number)
  {
    return (new CustomerQuery($client, $number))->fetch();
  }
}
