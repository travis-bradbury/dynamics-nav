<?php

namespace tbradbury\DynamicsNav;

/**
 * Class DynamicsNavClient.
 */
class DynamicsNavClient implements DynamicsNavClientInterface
{

    /**
     * The base URL to use for all endpoints.
     *
     * @var string
     */
    protected $baseUrl;

    /**
     * The username for authenticating with Nav.
     *
     * @var string
     */
    protected $user = '';

    /**
     * The password for authenticating with Nav.
     *
     * @var string
     */
    protected $password = '';

    /**
     * The company to use in Nav.
     *
     * @var string
     */
    protected $company;

    /**
     * The certificate to trust when connecting.
     *
     * @var string
     */
    protected $cafile = 'publiccert.pem';

    /**
     * DynamicsTest constructor.
     *
     * @param string $base_url
     *   The base URL for all endpoints.
     * @param string $username
     *   The username for basic authentication.
     * @param string $password
     *   The password for basic authentication.
     * @param string $company
     *   The company to use.
     */
    public function __construct($base_url, $username, $password, $company)
    {
        $this->baseUrl = $base_url;
        $this->user = $username;
        $this->password = $password;
        $this->company = $company;
    }

    /**
     * Test connection to Dynamics Nav.
     *
     * @return bool
     *   True if connection is successful.
     */
    public function test_connection()
    {
        $headers = $this->set_headers();
        $connection = $this->get_resource($this->baseUrl, $headers);
        $connection = json_decode($connection);

        return (isset($connection->value) && count($connection->value) > 0) ? TRUE : FALSE;
    }

    /**
     * Get a new instance with a different company.
     *
     * @param string $company
     *   The new company to use.
     *
     * @return static
     */
    public function withCompany($company)
    {
        return new static($this->baseUrl, $this->user, $this->password, $company);
    }

    /**
     * Send a request.
     *
     * Uses POST if $data is provided. Uses GET otherwise.
     *
     * @param string $url
     *   The URL to request.
     * @param mixed $data
     *   Data to POST to the API.
     *
     * @return mixed
     *   The response from the API.
     */
    public function request($url, $data = null)
    {
        $headers = $this->set_headers(['Company: ' . $this->company]);
        $response = $this->get_resource($url, $headers, $data);

        return $response;
    }

    /**
     * Set request headers.
     *
     * @param $options
     *   Optional, additional headers to set.
     *
     * @return array
     *   Request headers.
     */
    protected function set_headers($options = [])
    {
        $headers = [
            'Method: POST',
            'Connection: Keep-Alive',
            'User-Agent: DynamicsNavClient (PHP)',
            'Content-Type: application/json; charset=utf-8',
            'Accept: application/json',
        ];

        return array_merge($headers, $options);
    }

    /**
     * Get response from the remote server.
     *
     * @param $url
     *   URL to get response from.
     * @param $headers
     *   Request headers.
     * @param $data
     *   Optional data to post.
     *
     * @return array
     *   Response from remote server.
     */
    protected function get_resource($url, $headers, $data = null)
    {
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        if ($data) {
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        }
        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($ch, CURLOPT_CAINFO, DRUPAL_ROOT . '/cert' /** __DIR__ . */  . '/' . $this->cafile);
        curl_setopt($ch, CURLOPT_USERPWD, $this->user . ':' . $this->password);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
        $response = curl_exec($ch);

        if (!empty($err = curl_error($ch))) {
            //TODO: log cURL errors.
          watchdog('error', '<pre>' . print_r($err, TRUE) . '</pre>');
        }

        return $response;
    }

    /**
     * Get the API's base URL.
     *
     * @return string
     *   The base URL for the API.
     */
    public function baseUrl()
    {
        return $this->baseUrl;
    }

    /**
     * Get the URL for a path.
     *
     * @param string $path
     *   The path to request.
     *
     * @return string
     *   The complete URL.
     */
    public function url($path)
    {
        return $this->baseUrl() . $path;
    }

}
